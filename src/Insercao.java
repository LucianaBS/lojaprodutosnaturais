
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author aluno
 */
public class Insercao {

    public void inserirCliente(String nome_cli, String email_cli, String rg_cli, String telefone_cli, String cpf_cli) {
        try {
            try (Connection c = Conexao.obterConexao()) {
                PreparedStatement ps = c.prepareStatement("insert into sc_luciana.cliente(nome_cli, email_cli, rg_cli, telefone_cli,cpf_cli) values (?,?,?,?,?)");
                ps.setString(1, nome_cli);
                ps.setString(2, email_cli);
                ps.setString(3, rg_cli);
                ps.setString(4, telefone_cli);
                ps.setString(5, cpf_cli);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inserirComerciante(String nome_comerc, String email_comerc, String rg_comerc, String telefone_comerc, String cpf_comerc, String num_comerc) {
        try {
            try (Connection c = Conexao.obterConexao()) {
                PreparedStatement ps = c.prepareStatement("insert into sc_luciana.comerciante(nome_comerc, email_comerc, rg_comerc, telefone_comerc, cpf_comerc, num_cadastro) values (?,?,?,?,?,?)");
                ps.setString(1, nome_comerc);
                ps.setString(2, email_comerc);
                ps.setString(3, rg_comerc);
                ps.setString(4, telefone_comerc);
                ps.setString(5, cpf_comerc);
                ps.setString(6, num_comerc);
                ps.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void inserir(String text, String text0, String text1, String text2, String text3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void InserirProduto(String nome_produto, String codigo_produto, String qtd_produto) {
        try {
            Connection C = Conexao.obterConexao();
            String SQL = "Insert into sc_luciana.produto(nome_produto,codigo_produto,qtd_produto)values(?,?,?);";
            PreparedStatement s = C.prepareStatement(SQL);
            s.setString(1, nome_produto);
            s.setString(2, codigo_produto);
            s.setString(3, qtd_produto);
            s.executeUpdate();
            C.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void InserirFornecedor(String nome_fornec, String cpf_fornec, String rg_fornec, String telefone_fornec, String email_fornec, int codigo_produto) {
        try {
            Connection C = Conexao.obterConexao();
            String SQL = "Insert into sc_luciana.fornecedor(nome_fornec,cpf_fornec,rg_fornec,telefone_fornec,email_fornec,codigo_produto)values(?,?,?,?,?,?);";
            PreparedStatement s = C.prepareStatement(SQL);
            s.setString(1, nome_fornec);
            s.setString(2, cpf_fornec);
            s.setString(3, rg_fornec);
            s.setString(4, telefone_fornec);
            s.setString(5, email_fornec);
            s.setInt(6, codigo_produto);
            s.executeUpdate();
            C.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selecaoFornecedor(JComboBox combo) {
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.obterConexao();
            PreparedStatement p = c.prepareStatement("select * from sc_luciana.produto");
            ResultSet rs = p.executeQuery();
            while(rs.next())
                    {
                        ProdutoClasse p1 = new ProdutoClasse(rs.getInt("codigo_produto"));
                        System.out.println(p1);
                        m.addElement(p1);
                    }
                    combo.setModel(m);
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
