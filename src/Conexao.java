

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

    public static Connection obterConexao() {
        try {
            Class.forName("org.postgresql.Driver");
            String usuario = "luciana_baeta";
            String senha = "luciana_baeta";
            String banco = "jdbc:postgresql://10.90.24.54/luciana_baeta";
            return DriverManager.getConnection(banco,usuario, senha );
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
