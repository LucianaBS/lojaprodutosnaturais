/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aluno
 */
public class ProdutoClasse {
    private int cod_produto;

    public ProdutoClasse(int cod_produto) {
        this.cod_produto = cod_produto;
    }

    public int getCod_produto() {
        return cod_produto;
    }

    public void setCod_produto(int cod_produto) {
        this.cod_produto = cod_produto;
    }

    @Override
    public String toString() {
        return cod_produto + "";
    }
    
}
